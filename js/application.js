// Whatup Jquery
$(function(){
    sdms.init();
});

var sdms = (function($, document, window, undefined){
    
    // app configuration
    config = {
        api_url: 'http://sdms.sapienttoronto.com/cms/services/rest/',
        enable_cookies: true,
        cookie_lifetime: 1    // in days
    }
    
    // Cache selectors
    sel = {
        pages : {
            $login: $('#login'),
            $devices: $('#devices'),
            $borrowers: $('#borrowers'),
            $checkout: $('#checkout'),
            $checkin: $('#check_in')
        },
        $deviceList: $('#device_list'),
        $borrowerList: $('#borrower_list'),
        $checkoutList: $('#checkout_list'),
        $checkinList: $('#check_in_list')
    },
    
    // Create application state object
    state = {
        loading: false,
        user_data: false,
        page: 'login',
        todayStr: false,
        checkout: {
            device: false,
            device_title: false,
            borrower: false,
            borrower_title: false,
            reason: false,
            start: false,
            end: false
        }
    },
    
    // Kick this baby off...
    init = function(){
        
        ////////////////////////////
        // Auth
        ////////////////////////////
        
        
        // if cookies are enabled check for existing
        // if not redirect to login
        if(config.enable_cookies){
            if($.cookie('sdms')){
                state.user_data = $.deparam.fragment($.cookie("sdms"));
            }else{
                logout();
            }
        }else{
            logout();
        }
        
        
        ////////////////////////////
        // Form Submit Handlers
        ////////////////////////////     
        
        $('#login_form').bind('submit', function(e){
            e.preventDefault();
            // show loading message and load page contents
            showLoading();
            api('user/login', 'post', false, "username="+$('#username').val()+"&password="+ $('#password').val(), function(response){
                //check if the authorization was successful or not
                if (response.sessid){
                    state.user_data = response.user;
                    if(config.enable_cookies){
                        $.cookie('sdms', $.param(state.user_data), { expires: config.cookie_lifetime });
                    }
                    $.mobile.changePage($('#devices'));
                    hideLoading();
                }else{
                    hideLoading();
                    navigator.notification.alert('Athentication failed, please try again...');
                }
            }, function(response){
                hideLoading();
                console.log(response);
                navigator.notification.alert('Request failed, please try again...');
            });
            
            return false;
        });
        
        $('#add_borrower_form').bind('submit', function(e){
            e.preventDefault();
            
            // show loading message and load page contents
            showLoading();
            
            // build borrower object for create request
            var borrower_name = $('#borrower_name').val();
            var data = '';
            data += '{'
            data += '"type":"borrower",';
            data += '"title":"'+borrower_name+'",';
            data += '"field_email":[{"value":"'+$('#borrower_email').val()+'"}],';
            data += '"field_home_city":[{"value":"'+$('#borrower_office').val()+'"}],';
            data += '"field_role":[{"value":"'+$('#borrower_role').val()+'"}]';
            data += '}';
            
            api('node', 'post', 'application/json; charset=utf-8', data, function(response){
                // check if borrower was created
                if (response.nid){
                    $.mobile.changePage($('#borrowers'));
                    hideLoading();
                    navigator.notification.alert('Borrower '+borrower_name+' added successfully!');
                }else{
                    hideLoading();
                    genericError();
                }
            }, function(response){
                hideLoading();
                genericError();
            });
            
            return false;
        });
        
        
        ////////////////////////////
        // Event Handlers
        ////////////////////////////
        
        // checkout save click
        $('#checkout_save').tap(checkoutSaveClick);
        
        // check-in save click
        $('#check_in_save').tap(checkinSaveClick);
        
        // device click
        sel.$deviceList.on('click', 'a', deviceClick);
        
        // borrower click
        sel.$borrowerList.on('click', 'a', borrowerClick);
        
        // QR button click
        $('.btn_qr_capture').tap(qrScan);
        
        // bind mobile init
        $(document).bind( "mobileinit", function() {
            // allow cross-domain xhr
            $.support.cors = true;
            $.mobile.allowCrossDomainPages = true;
        });
        
        // prevent clicks during loading
        $(document).click(isLoading);
        
        
        ////////////////////////////
        // Init Hash Change Management
        ////////////////////////////
        
        // hash change event
        
        $(window).hashchange(function(){
            
            state.page = location.hash.replace('#', '');
            
            // if we don't have login data
            // send them punks to login
            if(!state.user_data){
                if(state.page === 'login' || state.page === ''){
                    console.log('login here');
                }else{
                    logout();
                }
            }else{
                if(state.page === 'login' || state.page === ''){
                    location.hash = 'devices';
                }
                if(state.page === 'logout'){
                    logout();
                }
                // load device page
                if(state.page === "devices"){
                    // show loading message and load page contents
                    showLoading();
                    loadDevices(state.user_data.profile_home_office, false, false, function(data){
                        var temp_html,
                            html = $(document.createElement('ul')),
                            node;
                        // render devices from ich templates
                        for(device in data){
                            if(data[device].field_state[0].value === "IN"){
                                data[device].theme = 'a';
                                data[device].rel = '#borrowers';
                            }else{
                                data[device].theme = 'e';
                                data[device].rel = '#check_in';
                            }
                            node = ich.device(data[device]);
                            html.append(node);
                        }
                        // set jqm listview attrs
                        html.attr({'data-role': 'listview', 'data-theme': 'a', 'data-filter': 'true'});
                        
                        sel.$deviceList.html(html);
                        
                        // set $page
                        $page = sel.pages.$devices;
                        
                		// Pages are lazily enhanced. We call page() on the page
                		// element to make sure it is always enhanced before we
                		// attempt to enhance the listview markup we just injected.
                		// Subsequent calls to page() are ignored since a page/widget
                		// can only be enhanced once.
                		$page.page();
                		
                		// Enhance the listview we just injected.
                		sel.$deviceList.find('ul').listview();
                		
                		// Remove ui-content class from lists
                		// TODO: figure out bug that adds ui-content to lists
                		sel.$deviceList.removeClass('ui-content');
                		
                		// change page and hide loading message 
                        
                        $.mobile.changePage(sel.pages.$devices);
                        hideLoading();
                        
                    });
                }
                // load borrower page
                if(state.page === "borrowers"){
                    showLoading();
                    loadBorrowers(function(data){
                    
                        var html = $(document.createElement('ul')),
                            node;
                        // render borrowers from ich templates
                        for(borrower in data){
                            node = ich.borrower(data[borrower]);
                            html.append(node);
                        }
                        // set jqm listview attrs
                        html.attr({'data-role': 'listview', 'data-theme': 'a', 'data-filter': 'true'});
                        
                        sel.$borrowerList.html(html);
                        
                        // set $page
                        $page = sel.pages.$borrowers;
                        
                        // Pages are lazily enhanced. We call page() on the page
                        // element to make sure it is always enhanced before we
                        // attempt to enhance the listview markup we just injected.
                        // Subsequent calls to page() are ignored since a page/widget
                        // can only be enhanced once.
                        $page.page();
                        
                        // Enhance the listview we just injected.
                        sel.$borrowerList.find('ul').listview();
                        
                        // Remove ui-content class from lists
                        // TODO: figure out bug that adds ui-content to lists
                        sel.$borrowerList.removeClass('ui-content');
                        
                        // change page and hide loading message 
                        
                        $.mobile.changePage(sel.pages.$borrowers);
                        hideLoading();
                    }); 
                }
                // load checkout page
                if(state.page === "checkout"){
                    // show loading message and load page contents
                    showLoading();
                    if((!state.checkout.borrower) || (!state.checkout.device)){
                        location.hash = 'devices';
                    }else{
                        var html = $(document.createElement('ul')),
                            device,
                            borrower;
                        // render device and borrower
                        html.append('<li class="ui-btn ui-li ui-btn-up-a">'+sel.$deviceList.find('[data-id='+state.checkout.device+']').html()+'</li>');
                        html.append('<li class="ui-btn ui-li ui-btn-up-a">'+sel.$borrowerList.find('[data-id='+state.checkout.borrower+']').html()+'</li>');
                        // set jqm listview attrs
                        html.attr({'data-role': 'listview', 'data-theme': 'a'});
                        
                        sel.$checkoutList.find('ul').replaceWith(html);
                        
                        // set $page
                        $page = sel.pages.$checkout;
                        
                        // Pages are lazily enhanced. We call page() on the page
                        // element to make sure it is always enhanced before we
                        // attempt to enhance the listview markup we just injected.
                        // Subsequent calls to page() are ignored since a page/widget
                        // can only be enhanced once.
                        $page.page();
                        
                        // Enhance the listview we just injected.
                        sel.$checkoutList.find('ul').listview();
                        
                        // change page and hide loading message 
                        
                        $.mobile.changePage(sel.pages.$checkout);
                        hideLoading();
                    }
                }
                
                // load check-in page
                if(state.page === "check_in"){
                    // show loading message and load page contents
                    showLoading();
                    if(!state.checkout.device){
                        location.hash = 'devices';
                    }else{
                        var html = $(document.createElement('ul')),
                            device;
                        // render device and borrower
                        html.append('<li class="ui-btn ui-li ui-btn-up-a">'+sel.$deviceList.find('[data-id='+state.checkout.device+']').html()+'</li>');
                        // set jqm listview attrs
                        html.attr({'data-role': 'listview', 'data-theme': 'a'});
                        
                        sel.$checkinList.find('ul').replaceWith(html);
                        
                        // set $page
                        $page = sel.pages.$checkin;
                        
                        // Pages are lazily enhanced. We call page() on the page
                        // element to make sure it is always enhanced before we
                        // attempt to enhance the listview markup we just injected.
                        // Subsequent calls to page() are ignored since a page/widget
                        // can only be enhanced once.
                        $page.page();
                        
                        // Enhance the listview we just injected.
                        sel.$checkinList.find('ul').listview();
                        
                        // change page and hide loading message 
                        $.mobile.changePage(sel.pages.$checkin);
                        hideLoading();
                    }
                }
            }
        });
        
        // fire initial hashchange event to register inital page
        $(window).hashchange();
        
        ////////////////////////////
        // MISC
        ////////////////////////////
        
        // generate today's date in api friendly format
        var now = new Date();
        state.todayStr = now.getDate()+'.'+(now.getMonth()+1)+'.'+now.getFullYear();
        
        // set default value for checkout date
        $('#checkout_date').val(state.todayStr);
        
    },
    
    // log the user out of the application
    logout = function(){
        api('user/logout', 'post', false, false, logoutCallback, logoutCallback);
    },
    
    // delete state data and cookie
    // redirect to login
    logoutCallback = function(){
        showLoading();
        state.user_data = false;
        if(config.enable_cookies){
            $.cookie('sdms', null);
        }
        $.mobile.changePage(sel.pages.$login);
        state.page = "login";
        hideLoading();
    },
    
    // confirm dialog callback for QR scan
    qrDialogCallback = function(button){
        if (button == 1){
            qrScan();
        }
    },
    
    // this method handles all the api calls and callbacks
    api = function(url, type, contentType, data, okay, nope){
        if(!contentType){
            contentType = 'application/x-www-form-urlencoded';
        }
        $.ajax({
            type: type,
            url: config.api_url + url + '.json',
            data: data,    
            dataType: 'json',
            contentType: contentType,
            beforeSend: function(xhr) {
                xhr.overrideMimeType("application/x-www-form-urlencoded");
            },
            success: function (response){
                okay(response);
            },
            error: function (response){
                nope(response);
            }
        });
    },
    
    // update app state with current device selected
    deviceClick = function(){
        var $device = $(this);
        state.checkout.device = parseInt($device.attr('data-id'));
        state.checkout.device_title = $device.attr('data-name');
    },
    
    // update app state with current borrower selected
    borrowerClick = function(){
        state.checkout.borrower = parseInt($(this).attr('data-id'));
        state.checkout.borrower_title = $(this).attr('data-name');
    },
    
    // load all devices
    loadDevices = function(office, state, asset, callback){
        var url, 
            args = '';
        
        // if asset tag is passed load only that device
        // if not, load all devices local to logged in user
        if(asset){
            url = 'views/devices_by_asset_tag';
            args = 'args[]='+asset
        }else{
            url = 'views/devices';
            if(office){
                args += 'args[]='+office  
                if(state){
                    args += '&'
                }
            }
            if(state){
                args += 'args[]='+state
            }
        }
        api(url, 'get', false, args, function(response){
            callback(response);
        }, function(response){
            console.log('error');
            return 'error';
        });
    },
    
    // Load all borrowers local to the logged in user
    loadBorrowers = function(callback){
        api('views/borrowers', 'get', false, false, function(response){
            console.log(response);
            callback(response);
        }, function(response){
            return 'error';
        });
    },
    
    // Load all rentals
    loadRentals = function(callback){
        api('views/rentals', 'get', false, false, function(response){
            callback(response);
        }, function(response){
            return 'error';
        });
    },
    
    // Handle device check out call
    checkoutDevice = function(callback){
        // Check for lack of title
        if(!state.checkout.reason){
            state.checkout.reason = 'No apparent reason';
        }
        
        // build request data for checkout request
        var data = '';
        data += '{'
        data += '"type":"rental",';
        data += '"title":"'+state.checkout.reason+'",';
        data += '"field_borrower":[{"nid":"'+state.checkout.borrower+'"}],';
        data += '"field_device":[{"nid":"'+state.checkout.device+'"}],';
        data += '"field_lease_duration":[{"value":{"date":"'+state.checkout.start+'"}, "value2":{"date":"'+state.checkout.end+'"}}]';
        data += '}';
        
        api('node', 'post', 'application/json; charset=utf-8', data, function(response){
            callback(response);
        }, function(response){
            genericError();
        });
    },
    // Handle device check in call
    checkinDevice = function(callback){
        var rental_id = false;
        
        // First load all rentals  
        // TODO: check-in device by device nid instead of rental nid
        loadRentals(function(response){
            
            // Check rentals for selected device 
            for(rental in response){
                if(response[rental].field_device[0].nid == state.checkout.device){
                    rental_id = response[rental].nid;
                }
            }
            
            // If device is currently in a rental, check it in
            if(rental_id){
                // check for lack of title
                if(!state.checkout.reason){
                    state.checkout.reason = 'No apparent reason';
                }
                
                // build request data for check-in request
                var data = '';
                data += '{'
                data += '"type":"return",';
                data += '"title":"'+state.checkout.reason+'",';
                data += '"field_rental":[{"nid":"'+rental_id+'"}]';
                data += '}';
                api('node', 'post', 'application/json; charset=utf-8', data, function(response){
                    callback(response);
                }, function(response){
                    navigator.notification.alert('Request failed, please try again...');
                });
            }else{
                navigator.notification.alert('Request failed, please try again...');
            }
        });
    },
    
    // Click handler for check out save button
    checkoutSaveClick = function(){
        state.checkout.reason = $('#checkout_reason').val();
        state.checkout.start = $('#checkout_date').val();
        state.checkout.end = $('#checkout_return').val();
        checkoutDevice(function(data){
            console.log(data);
            location.hash = 'devices';
            $('#checkout_date').val(state.todayStr);
            $('#checkout_return').val('');
            $('#checkout_reason').val('');
            state.checkout.reason = false;
            console.log('Success! '+ state.checkout.device_title + ' checked out to ' + state.checkout.borrower_title);
            navigator.notification.alert('Success! '+ state.checkout.device_title + ' checked out to ' + state.checkout.borrower_title);
            
        });
        return false;
    },
    
    // Click handler for check in save button
    checkinSaveClick = function(){
        state.checkout.reason = $('#check_in_reason').val();
        checkinDevice(function(data){
            console.log(data);
            location.hash = 'devices';
            $('#check_in_reason').val('');
            state.checkout.reason = false;
            console.log('Success! '+ state.checkout.device_title + ' checked in!');
            navigator.notification.alert('Success! '+ state.checkout.device_title + ' checked in!');
        });
        return false;
    },
        
    // Call this function to fire QR Scanning
    qrScan = function(){
        // launch scanner
        window.plugins.barcodeScanner.scan(
            function(result){
                if (result.cancelled){
                    // Cancelled case
                    $('.btn_device_list', '#devices').click();
                }else{
                    if(result.text){
                        console.log(result.text);
                        // Success case
                        loadDevices(false, false, result.text, function(response){
                            console.log(response);
                            state.checkout.device = response[0].nid;
                            state.checkout.device_title = response[0].title;
                            var $navbarGrid = $('#devices').find('.ui-grid-a');
                            $('.ui-block-a a', navbarGrid).addClass('ui-btn-active');
                            $('.ui-block-b a', navbarGrid).removeClass('ui-btn-active');
                            if(response[0].field_state[0].value === "OUT"){
                                location.hash = 'check_in';
                            }else{
                                location.hash = 'borrowers';
                            }
                        });
                    }else{
                        // Error case
                        navigator.notification.checkout(
                            "There was an issue reading the device's asset tag.",
                            qrDialogCallback,
                            "Failure!",
                            "Retry,Cancel"
                        );
                    }
                }
            },
            function(error) {
                navigator.notification.alert('Scan failed: ' + error);
            }
        );
        return false;
    },
    
    // Pretty self explanatory
    genericError = function(){
        navigator.notification.alert('Something went wrong, please try again...');
    },
    
//     handle loading messages
    showLoading = function(){
        $.mobile.showPageLoadingMsg();
        state.loading = true;
    },
    
    hideLoading = function(){
        $.mobile.hidePageLoadingMsg();
        state.loading = false;
    },
    
    isLoading = function(e){
        if(state.loading){
            e.preventDefault();
            return false;
        }
    };
    
    // expose public methods/props
    return {
    	init : init
    }
        	
}(jQuery, document, window, undefined));